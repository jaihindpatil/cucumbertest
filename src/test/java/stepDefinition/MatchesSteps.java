package stepDefinition;


import bettingapp.ScoresPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import qa.factory.DriverFactory;

public class MatchesSteps {
	private static String title;
	private ScoresPage scoresPage = new ScoresPage(DriverFactory.getDriver());


@Given("User is on Scores & Fixtures - Football - BBC Sport Page")
public void user_is_on_scores_fixtures_football_bbc_sport_page() {
	System.out.println("Step 1: Scores & Fixtures - Football - BBC Sport Page");
	DriverFactory.getDriver().get("https://www.bbc.co.uk/sport/football/scores-fixtures");

	title = scoresPage.getPageTitle();
	System.out.println("Page title is: " + title);
}

@When("User select day as Today")
public void user_select_day_as_today() {
	System.out.println("Step 2: User click on Today");
	scoresPage.clickOnToday();
}

@Then("User should get all team names playing today")
public void user_should_get_all_team_names_playing_today() {
	System.out.println("Step 3: Verify number of leagues playing today");
	scoresPage.verifyNumLeagues();
	System.out.println("Step 4: Verify all team names playing today");
	
	scoresPage.getTeamNames();
}

@And("if no matches then message should be displayed")
public void if_no_matches_then_message_should_be_displayed() {
   
	System.out.println("Step 5: Verify message if no matches played today");
	scoresPage.verifyNoLeagues();
}
}