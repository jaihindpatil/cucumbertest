@Login

Feature: As a QA, I would like to verify all negative scenarios for login

 Scenario Outline: UnSuccessfully Login with Credentials
   Given user is on Home Page
   When user click sign in Link on Home Page
   And Fill UserID field with "<userid>" and Fill Password field with "<password>" from list and click Login on LoginPage
   Then user should not login
   Examples:Invalid User
     | userid    | password |
     | Abcde_123  | P@ssw0rd |
     | 1234549    | P@ssw0rd |