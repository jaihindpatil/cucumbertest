package bettingapp;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ScoresPage {
	private WebDriver driver;

	// 1. By Locators: OR

	private By Today = By.xpath("//span[contains(text(),'Today')]");

	private By League = By.xpath("//span[@role='region']//div[@class='qa-match-block']");

	private By TeamName = By.xpath(
			"//span[@role='region']//div[@class='qa-match-block']//div[@class='sp-c-fixture__wrapper']//span[@class='sp-c-fixture__team-name-wrap']");

	// 2. Constructor of the page class:
	public ScoresPage(WebDriver driver) {
		this.driver = driver;
	}

	// 3. page actions: features(behavior) of the page the form of methods:

	public String getPageTitle() {

		return driver.getTitle();
	}

	public void clickOnToday() {

		WebDriverWait wait = new WebDriverWait(driver, 30);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='sp-timeline-current-dates']")));

		driver.findElement(By.xpath("//ul[@id='sp-timeline-current-dates']")).click();

	}

	public void verifyNumLeagues() {
		int leagueCount = driver.findElements(By.xpath("//span[@role='region']//div[@class='qa-match-block']")).size();

		if (leagueCount > 0) {
			System.out.println("Count of leagues are -> " + leagueCount);
		} else if (leagueCount == 0) {
			System.out.println("There are no matches today");
		}
	}

	public void verifyNoLeagues() {
		int leagueCount = driver.findElements(By.xpath("//span[@role='region']//div[@class='qa-match-block']")).size();
		if (leagueCount > 0) {
			System.out.println(
					"Matches going to played today If there are no matches today different message will be displayed here.");
		} else if (leagueCount == 0) {
			System.out.println("There are no matches today");
		}
	}

	public void getTeamNames() {
		List<WebElement> teams = driver.findElements(By.xpath(
				"//span[@role='region']//div[@class='qa-match-block']//div[@class='sp-c-fixture__wrapper']//span[@class='sp-c-fixture__team-name-wrap']"));
		for (WebElement teamName : teams) {
			System.out.println("Team -> " + teamName.getText());
		}
	}

}