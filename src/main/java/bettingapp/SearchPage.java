package bettingapp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage {
	private WebDriver driver;

	// 1. By Locators: OR
	private By SearchIcon = By.xpath("//a[@href='/search']");
	private By SearchField = By.id("search-input");

	// 2. Constructor of the page class:
	public SearchPage(WebDriver driver) {
		this.driver = driver;
	}

	// 3. page actions: features(behavior) of the page the form of methods:

	public String getPageTitle() {

		return driver.getTitle();
	}

	public void searchArticles(String article) {
		driver.findElement(SearchIcon).click();

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search-input")));

		driver.findElement(SearchField).sendKeys(article);
		;

		driver.findElement(SearchField).submit();

	}

	public String getFirstHeading() {

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("(//ul[@role='list']//li//a//span[@role='text']//p)[1]")));

		String heading = driver.findElement(By.xpath("(//ul[@role='list']//li//a//span[@role='text']//p)[1]"))
				.getText();
		return heading;
	}

	public String getLastHeading() {

		String heading = driver.findElement(By.xpath("(//ul[@role='list']//li//a//span[@role='text']//p)[last()]"))
				.getText();
		return heading;

	}

}