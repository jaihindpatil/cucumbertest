package bettingapp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	private WebDriver driver;

	// 1. By Locators: OR

	private By SignInLink = By.xpath("//ul[@role='list']//a[contains(@href,'session.bbc')]");
	private By ErrMsgText = By.xpath("//p[@class='form-message__text']//span//span");

	// 2. Constructor of the page class:
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	// 3. page actions: features(behavior) of the page the form of methods:

	public String getPageTitle() {

		return driver.getTitle();
	}

	public void clickOnSignIN() {
		driver.findElement(SignInLink).click();
	}

	public void tryLoginWithInvalid(String username, String Passwd) {

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("user-identifier-input")));

		driver.findElement(By.id("user-identifier-input")).sendKeys(username);

		driver.findElement(By.id("password-input")).sendKeys(Passwd);

		// click signin button
		driver.findElement(By.id("submit-button")).click();

	}

	public String ErrorTextMessage() {
		return driver.findElement(ErrMsgText).getText();
	}
}