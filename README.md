# SportScoreTest

Automation of  test scenarios for application using cucumber java ,BDD framework.

Maven is a software project management and comprehension tool, so in other words it can be used to manage all software cycle.

**How to build**
- Eclipse - The following instructions are tested for Eclipse .

Clone git repository or download the maven project
Start eclipse
Import maven project into eclipse by select "File" → "Import..."
Write "maven" in "Select an import source" field, select "Existing Maven Projects" and then click on "Next >"
Browse to the directory where you save the project and then click on "Finish"
Click with the right mouse button on the root project and select "Run As" → "Maven build"
Run individual feature file.
there are 3 feature files for 3 tasks. 
Matches.feature
SearchArticle.feature
Login.feature
Right Click on Matches.feature & go to Run configuration to configure .
Select Matches.feature & click on run.

It will run Matches.feature scenario's.
Similarly we can triggers SearchArticle.feature & Login.feature scenario's.


- Another way is run project as maven project . USe mvn clean test goal to trigger 3 feature files.
It will run & generate report .



